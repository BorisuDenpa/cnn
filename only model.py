from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D, Merge, ZeroPadding2D, UpSampling2D
from keras.optimizers import SGD


# This one could help you to match and review the dimensions and parameters of every layer
# input: 224x224 images
# 1 Deep branch
input_shape = (224, 224, 1)

model = Sequential()
model.add(Conv2D(64, (3, 3), strides=1, input_shape=input_shape, activation='relu', padding='same'))
model.add(Conv2D(64, (3, 3), strides=1, activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Conv2D(128, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), strides=1, activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Conv2D(256, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), strides=1, activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=1))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), strides=1, activation='relu', padding='same'))

print("#Deep net")
# Summary gets info about the model, like dimensions, number of parameters and stuff
print(model.summary())

# 2 Shallow branch
model2 = Sequential()
model2.add(ZeroPadding2D(padding=(1, 1), input_shape=input_shape))
model2.add(Conv2D(24, (5, 5), strides=1, activation='relu', padding='same'))
model2.add(AveragePooling2D(pool_size=(5, 5), strides=2))
model2.add(ZeroPadding2D(padding=3))
model2.add(Conv2D(24, (5, 5), strides=1, activation='relu', padding='same'))
model2.add(AveragePooling2D(pool_size=(5, 5), strides=2))
model2.add(Conv2D(24, (5, 5), strides=1, activation='relu', padding='same'))
model2.add(AveragePooling2D(pool_size=(5, 5), strides=2))

print("#Shallow net")
print(model2.summary())

# 3 Join both branches
added = Sequential()
added.add(Merge([model, model2], mode='concat'))
added.add(Conv2D(4, (3, 3), strides=2, activation='relu', padding='same'))
added.add(UpSampling2D(size=2))
added.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
added.add(UpSampling2D(size=2))
added.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
added.add(UpSampling2D(size=2))
added.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
added.add(UpSampling2D(size=2))
added.add(Conv2D(1, (3, 3), activation='relu', padding='same'))

print("#Convoluted net")
print(added.summary())

# 4 This one compiles the model, leaves it ready for training
added.compile(loss='mean_squared_error', optimizer=SGD(lr=0.0000001, momentum=0.9), metrics=['accuracy'])

print("--compiled--")

